Title:  Projets à soutenir
Category: Pages
Slug: soutien-projets
Author: Tadorne
Summary: Une liste de projets et des liens pour faire des dons.

Vous trouverez ici une sélection subjective de projets et initiatives à soutenir dans les domaines de :
- la protection de la nature et des animaux
- les logiciels libres

# Protection de la nature et des animaux

## Volée de piafs

Centre de sauvegarde de la faune sauvage à Languidic, près de Lorient

[Donner à Volée de piafs via HelloAsso][0]

# Logiciels Libres

## Palemoon

Palemoon est un navigateur internet basé sur Firefox, qui poursuit actuellement sa propre trajectoire, en cherchant à être léger et utilisant une interface "classique" bien différente de la tendance à l'uniformisation introduite notamment par Google Chrome et reprise par Firefox et Internet Explorer. Par ailleurs, un objectif de l'équipe derrière Palemoon est de créer une plateforme basée sur la technologie XUL que Mozilla abandonne progressivement.

[Soutenir Palemoon via Ko-Fi][1]

## ReactOS

ReactOS est un système d'exploitation libre permettant d'exécuter des programmes Windows. Développé depuis plus de 15 ans, il est encore en version Alpha mais permet une compatibilité avec Windows 2003.

[Donner via Paypal ou virement bancaire][2]

# Window Maker Live

Window Maker Live est une version live et installable de Linux basée sur Debian Jessie avec Window Maker comme interface graphique par défaut, préconfigurée. wmlive a été pensée pour prendre en charge les ultraportables Thinkpad d'anciennes générations, en intégrant le support de matériel spécifique.

[Donner via Paypal][3] __choisir "envoyer de l'argent à la famille ou des amis"__

[0]: https://www.helloasso.com/associations/volee-de-piafs/formulaires/1/widget
[1]: https://ko-fi.com/palemoon
[2]: https://reactos.org/donating
[3]: https://www.paypal.me/wmlive
