Title: En vrac du Mercredi
Date: 2018-03-28 19:00
Modified: 2018-03-28 19:00
Category: En vrac
Tags: Windows 95, Linky, intelligence artificielle, Linux, animaux sauvages, nature
Slug: en-vrac-28-03-18
Author: Tadorne
Summary: Liens en vrac autour du numérique et de l'écologie.

# En vrac du Mercredi

## numérique

[Vieux Geek, épisode 117 : Les Powertoys pour MS-Windows 95][0]  
Un retour sur les Powertoys, des outils rajoutant des fonctionnalité avancées à Windows 95 puis ses successeurs jusqu'à XP. La vidéo de présentation permet de replonger dans des souvenirs de "customization" du système pour celles et ceux qui l'ont pratiqué.

[CPC Hardware n°36 débarque en kiosque][1]  
Nouveau numéro de la revue Canard PC Hardware, qui fête son neuvième anniversaire. On y trouvera notamment des infos fraîches sur le fameux compteur Linky, les "technologies du futur" ou encore un test des processeurs 486 DX2/DX4 et Pentium (1ère génération)...

[Luis Perez-Breva (MIT) : "Sans vouloir vous alarmer, vous employez probablement mal le terme d'intelligence artificielle"][2]  
Des éclaircissements sur l'intelligence artificielle, ce qu'elle est, ce qu'elle n'est pas.

[How to Easily Read a Linux Man Page][3]  
Les pages __man__ (comme __manual__) sont des documents associés à différents commandes et logiciels sous linux. Elles sont couramment accédées via l'interface de ligne de commande. L'interface étant quelque peu austère, cet article permet de mieux en exploiter le potentiel. Par ailleurs, comme il est rappelé, la commande `man man` permet de creuser le sujet...

## écologie

[Aux Sept-Îles, le Macareux moine revient pour nicher][a]  
La LPO (Ligue de Protection des Oiseaux) a été créée en 1912 contre le massacre des macareux moine lors de "safaris" en Bretagne. Dans la foulée est créée la réserve des 7 îles, qu'elle gère depuis cette date. A partir du 7 avril, l'oiseau y revient nicher pour se reproduire et nourrir son poussin. Il est possible d'en apprendre plus sur le volatile à l'espace exposition de la station voisine de l'Ile Grande (également centre de soins - non visitable). 

[Achetez utile – les opérations caddies de Volée de piafs][b]  
Le centre de sauvegarde de la faune sauvage Volée de piafs organise des "opérations caddies", mettant à contribution les clientes et client de supermarchés locaux pour faire remonter les stocks de nourriture et de fournitures pour les soins des animaux sauvages. C'est aussi un temps de sensibilisation sur l'activité de l'association et les enjeux de préservation de la biodiversité.

[Levons le pied pour les crapauds amoureux !][c]  
L'ASPAS (Association pour la Protection des Animaux Sauvages) donne des conseils pour limiter la mortalité des amphibiens qui traversent les routes pour rejoindre leurs lieux de reproduction.

* La rubrique ["en vrac"][4] a été lancée par [Tristan Nitot][5] dans son [Standblog][6] avant d'être reprise ailleurs sur le web. C'est une sélection de liens d'actualité en rapport avec les sujets abordés dans un blog.

[0]: <http://frederic.bezies.free.fr/blog/?p=17411>
[1]: <https://www.cpchardware.com/cpc-hardware-36-debarque-en-kiosque/>
[2]: <http://www.zdnet.fr/actualites/luis-perez-breva-mit-sans-vouloir-vous-alarmer-vous-employez-probablement-mal-le-terme-d-intelligence-artificielle-39866006.htm>
[3]: <https://www.maketecheasier.com/read-linux-man-page/>
[4]: <https://standblog.org/blog/category/En-vrac>
[5]: <https://fr.wikipedia.org/wiki/Tristan_nitot>
[6]: <https://standblog.org/blog/category/En-vrac>
[a]: <https://www.lpo.fr/actualites/aux-sept-iles-le-macareux-moine-revient-pour-nicher-dp1>
[b]: <https://volee-de-piafs.fr/financement/operations-caddies-volee-de-piafs/>
[c]: <https://www.aspas-nature.org/slider/levons-pied-crapauds-amoureux/> 
