Title:  Nouveau site pour l'association Volée de piafs
Date: 2018-03-21 10:00
Modified: 2018-03-21 10:00
Category: Ecologie
Tags: animaux, Bretagne, nature
Slug: nouveau-site-volee-de-piafs
Author: Tadorne
Summary: Volée de piafs, centre de sauvegarde de la faune sauvage près de Lorient a ouvert un nouveau site. Pour les associations, la communication sur Internet est cruciale.

#Volée de piafs prépare son nouveau site

## Présentation de l'association Volée de piafs

Volée de piaf est un centre de sauvegarde de la faune sauvage. L'association soigne des oiseaux, mais aussi d'autres animaux sauvages amenés par des particuliers (appelés découvreurs) ou via des bénévoles et vétérinaires partenaires. Les animaux accueillis au centre le sont  en raison de blessures, de maladies, ou car victimes d'empoisonnement. S'il est basé à Languidic près de Lorient, l'action du centre s'étend sur une grande partie de la Bretagne. En 2016, 3 000 animaux sont passés par le centre.
Par son action, Volée de piafs participe aussi à sensibiliser le public autour des questions de biodiversité. 

Pour garantir son fonctionnement, l'association doit régulièrement chercher du soutien financier et mobiliser des bénévoles. A ce titre, la communication sur internet est un aspect qui ne peut être négligé.

## Evolution de sa présence sur le web

Jusque cette année, Volée de Piafs avait un blog pour communiquer (hébergé sur une plateforme commerciale [propriétaire][0], avec en parallèle une [page Facebook][1] mise à jour plus régulièrement.

L'association a lancé un nouveau site plus attractif et fonctionnel (possibilité d'adhérer ou de donner en ligne) avec en ligne de mire des publications plus régulières. On y trouve les actualité de l'association, des informations sur son fonctionnement ou pour [être bénévole][2]

L'adresse du nouveau site de Volée de piafs :
[http://www.volee-de-piafs.fr/][3]

## Caractéristiques des supports utilisés :

Volée de Piafs s'est orientée vers l'ouverture d'un site sous Wordpress qu'elle maîtrise davantage que [l'ancien blog][4], tout en continuant de publier sur Facebook. Un choix que font beaucoup d'asociations. Toutefois, une tendance grandissante est à délaisser la publication sur les sites web au profit de la plateforme de réseau social établie à Menlo Park.

Sans rentrer dans le débat sur l'utilisation des données par Facebook, voici un tableau non-exhaustif des avantages et inconvénients des deux outils utilisés ici pour publier sur le web.


**Quelques caractéristiques des deux outils de publication**

|Facebook|  Site Web |
|-------------|-------------|
|Grande fréquentation de la plateforme, mais  Facebook bride de plus en plus la visibilité des publications aux personnes inscrites . |  Publications visibles sans restriction posée par une plateforme. Le risque existe au départ de ne pas petre visible dans les résultats des moteurs de recherche. Un travail de promotion de son site internet est nécessaire (publier régulièrement, avoir des liens vers son site).|
|Les visiteurs peuvent "s'abonner" via le fil d'actualité à condition d'avoir un compte sur la plateforme. Ce fil ne reprend que partiellement les nouvelles publications.  L'incitation à publier très souvent est très forte pour capter l'attention des personnes abonnées. Facebook pousse aussi à payer pour être affiché dans les flux d'actualité|Les visiteurs peuvent choisir la manière et le rythme pour suivre le site : marque-pages dans le navigateur, fils RSS, lettre d'information.Ces outils demandent souvent un apprentissage ou une redécouverte de leur maniement.|
| Possibilité d'entrer relativement facilement en contact (grande base d'utilisateurs). Cependant, les personnes non-inscrites n'ont pas l'autorisation de communiquer via la plateforme. |Contacts possibles par les moyens de son choix (formulaire sur une page, email, messagerie instantanée, affichage du numéro de téléphone ou d'une adresse postale...). Il n'existe par contre pas d'annuaire satisfaisant pour retrouver des contacts sur le web|
|Formulaire de publication minimaliste, et accès rapide du fait de l'incitation à être déjà connecté à la plateforme ou d'utiliser l'application sur "smartphone". Très fortes limitations sur le contenu publiable, sa modification ou pour le récupérer, le transférer... | L'outil peut être adapté à ses besoins : une infinité de formes et de contenus possibles... Par exemple [Frama.site][5] proposes quelques-unes (Blog, site collaboratif façon Wiki, Page de présentation) des multiple possibilités de publication sur le web. Outils moins intuitifs à manier en général, interfaces pour mobiles en général plus complexes. On sort davantage du "clé en main" avec la nécessité d'apprendre un peu plus sur le maniement de différents outils.|


[0]: https://fr.wikipedia.org/wiki/Logiciel_propri%C3%A9taire
[1]: https://www.facebook.com/voleedepiafs/
[2]: https://volee-de-piafs.fr/benevolat/etre-benevole-a-volee-de-piafs/
[3]: http://www.volee-de-piafs.fr/
[4]: http://voleedepiafs.eklablog.com/
[5]: https://frama.site/
